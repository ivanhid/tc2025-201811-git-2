# 2nd Git challenge #

In this repository there is work by parts in different branches,
review the history with `git log` (try also `git log --all --graph --decorate --oneline`).
Your work consists in generate a history as is everything was made over master,
`git log --oneline` in master would show something as:

* 535f5a1 instructions
* 73d65a0 conclusion
* 9fd9d9a develpment
* e052bb6 introduction
* 7486e97 base

Note that as you are rewriting the master history, your repository will become
incompatible with mine (why?).

